﻿using System;

namespace AutoCaller.Models
{
    public class Response
    {
        public bool Success { get; set; } = false;

        public string ErrorMessage { get; set; }

        public object RObjet { get; set; }

        public Exception Exp { get; set; }
    }
}

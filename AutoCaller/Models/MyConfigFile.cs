﻿using System;

namespace AutoCaller.Models
{
    public class MyConfigFile
    {
        public string DisplayName { get; set; }

        public string UserName { get; set; }

        public string RegisterName { get; set; }

        public string RegisterPassword { get; set; }

        public string DomainHost { get; set; }

        public int DomainPort { get; set; }

        public bool RegistrationRequired { get; set; }

        public MyConfigFile(
            string displayName,
            string userName, 
            string registerName, 
            string registerPassword, 
            string domainHost, 
            int domainPort,
            bool registrationRequired = true)
        {
            RegistrationRequired = registrationRequired;
            DisplayName = displayName ?? string.Empty;
            UserName = userName ?? string.Empty;
            RegisterName = registerName ?? string.Empty;
            RegisterPassword = registerPassword ?? string.Empty;
            DomainHost = domainHost ?? string.Empty;
            DomainPort = domainPort;
        }

        public MyConfigFile()
        {
            RegistrationRequired = false;
            DisplayName = string.Empty;
            UserName = string.Empty;
            RegisterName = string.Empty;
            RegisterPassword = string.Empty;
            DomainHost = string.Empty;
            DomainPort = 0;
        }
    }
}

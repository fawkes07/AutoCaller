﻿using System.ComponentModel;

namespace AutoCaller.Models
{
    public class DgvClientesModel
    {
        [DisplayName("Clave Promotor")]
        public string ClienteClave { get; set; }
        
        public string Nombre { get; set; }
        
        [DisplayName ("Telefono 1")]
        public string Telefono1 { get; set; }

        [DisplayName("Telefono 2")]
        public string Telefono2 { get; set; }

        [DisplayName("Pago requerido")]
        public decimal SaldoRequerido { get; set; }
        
        [DisplayName("Estatus de la llamada")]
        public string LlamadaEstatus { get; set; }

        public DgvClientesModel(
            string clienteClave, 
            string nombre, 
            string telefono1, 
            string telefono2,
            decimal saldoRequerido
            )
        {
            ClienteClave = clienteClave ?? string.Empty;
            Nombre = nombre ?? string.Empty;
            Telefono1 = telefono1;
            Telefono2 = telefono2;
            SaldoRequerido = saldoRequerido;
        }
    }
}

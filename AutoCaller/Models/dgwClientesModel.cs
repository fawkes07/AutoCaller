﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoCaller.Models
{
    public class DgwClientesModel
    {
        public string Nombre { get; set; }

        public string Telefono1 { get; set; }

        public string Telefono2 { get; set; }

        //public string Status { get; set; }

        public DgwClientesModel(string _nombre, string _telefono1, string _telefono2)
        {
            this.Nombre = _nombre;
            this.Telefono1 = _telefono1;
            this.Telefono2 = _telefono2;
            //this.Status = "En cola";
        }
        
    }
}

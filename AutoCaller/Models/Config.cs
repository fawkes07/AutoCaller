﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoCaller.Models
{
    public class Config
    {
        public bool RegistrationRequired { get; set; }

        public string DisplayName { get; set; }

        public string AuthenticationId { get; set; }

        public string UserName { get; set; }

        public string RegisterName { get; set; }

        public string RegisterPassword { get; set; }

        public string DomainHost { get; set; }

        public int DomainPort { get; set; }
        
        public Config()
        {
            
        }
    }
}

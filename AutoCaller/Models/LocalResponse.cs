﻿using System;

namespace AutoCaller.Models
{
    public class LocalResponse
    {
        public bool Success { get; set; } = false; 
 
        public string ErrorMessage { get; set; } 
 
        public object RObjet { get; set; } 
 
        public Exception Exception { get; set; }

        public LocalResponse(string errorMessage) 
        {
            ErrorMessage = errorMessage;
            Success = false;
        }

        public LocalResponse(Exception exeption, string errorMessage) 
            : this (errorMessage)
        {
            Exception = exeption;
        }

        public LocalResponse(object robject)
        {
            RObjet = robject;
            Success = true;
        }

        public LocalResponse()
        {
            Success = true;
        }
    }
}

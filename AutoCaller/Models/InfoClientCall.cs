﻿namespace AutoCaller.Models
{
    public class InfoClientCall
    {
        public DgvClientesModel Cliente { get; set; }

        public string Message { get; set; }
    }
}

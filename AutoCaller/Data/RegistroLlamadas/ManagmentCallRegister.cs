﻿using System;
using AutoCaller.Models;
using DataBase;
using static AutoCaller.Models.Enums;
using System.Data;

namespace AutoCaller.Data.RegistroLlamadas
{
    public static class ManagmentCallRegister
    {
        public static LocalResponse GetCallsToDo()
        {
            var response = new LocalResponse();

            string usrAutoCallerId = "13";
#if DEBUG
            usrAutoCallerId = "1";
#endif

            DataTable result;

            try
            {
                var dbConetion = new DB(ServidorBaseDatos.Oracle);
                dbConetion.AbrirBase();
                dbConetion.Consulta =
                    $@" SELECT * FROM TM_BSC_CILINDRO_LLAMADAS C
                    WHERE 
                        C.ESTATUS = '{1}' 
                    AND C.USUARIOS_GESTION like '%{usrAutoCallerId}%'
                    AND (LTRIM(RTRIM(C.TELEFONO1)) IS NOT NULL 
                        OR LTRIM(RTRIM(C.TELEFONO2)) IS NOT NULL)";

                result = dbConetion.GenerarValores(dbConetion.Consulta);
                dbConetion.CerrarBase();
            }
            catch (Exception e)
            {
                return new LocalResponse (e, "Error al conectar con la base de datos");
            }
            return new LocalResponse(result);
        }

        public static LocalResponse ChangeStatusCall(
            string clavePromotor, EstatusLlamada newStatus)
        {
            var dbConetion = new DB(ServidorBaseDatos.Oracle);

            try
            {
                dbConetion.AbrirBase();
                dbConetion.Consulta =
                    "UPDATE TM_BSC_CILINDRO_LLAMADAS " +
                    $"SET ESTATUS = '{((int) newStatus).ToString()}', " +
                    $"NOMBRE_USUARIO = 'AUTOCALLER', " +
                    $"USUARIO_GESTION = '2210', " +
                    $"FECHA_GESTION_INICIA = SYSDATE " +
                    $"WHERE CLAVE_PROMOTOR = '{clavePromotor}'";

                var result = dbConetion.EjecutarConsultaInsert();
            }
            catch (Exception e)
            {
                return new LocalResponse(e, e.Message);
            }
            finally
            {
                dbConetion.CerrarBase();
            }

            return new LocalResponse();
        }
    }
}

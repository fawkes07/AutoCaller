﻿using System;
using System.Collections.Generic;
using System.Threading;
using AutoCaller.Models;

namespace AutoCaller.Softphone
{
    internal class Autodialer
    {
        private readonly Softphone _softphone;
        private readonly List<InfoClientCall> _callList;
        private readonly int _maxConcurrentCall;
        private readonly AutoResetEvent _autoResetEvent;
        private readonly object _sync;
        public readonly List<CallHandler> CallHandlers;
        
        private int _currentConcurrentCall;
        private bool _continue = true;

        public Autodialer(
            Softphone softphone,
            List<InfoClientCall> callList, 
            int maxConcurrentCall)
        {
            _sync = new object();
            _softphone = softphone;
            _callList = callList;
            _maxConcurrentCall = maxConcurrentCall;
            CallHandlers = new List<CallHandler>();
            _autoResetEvent = new AutoResetEvent(false);
        }

        public void Start()
        {
            _continue = true;

            ThreadPool.QueueUserWorkItem(o =>
            {
                foreach (var callInfo in _callList)
                {
                    // Salida en caso de cambio de bandera
                    if (!_continue)
                        break;

                    if (_currentConcurrentCall < _maxConcurrentCall)
                    {
                        StartCallHandler(callInfo);
                    }
                    else
                    {
                        _autoResetEvent.WaitOne();
                        StartCallHandler(callInfo);
                    }
                }

                // TERMINA LA COLA DE LLAMADAS
                _softphone.Gui.LlamadasEnProgreso(false);
            });
        }

        public void Stop()
        {
            _continue = false;
        }

        private void StartCallHandler(InfoClientCall callInfo)
        {
            lock (_sync)
            {
                ++_currentConcurrentCall;
                var callHandler = new CallHandler(_softphone, callInfo);
                callHandler.Completed += CallHandler_Completed;
                CallHandlers.Add(callHandler);

                callHandler.Start();
            }
        }

        private void CallHandler_Completed(object sender, EventArgs e)
        {
            lock (_sync)
            {
                CallHandlers.Remove((CallHandler)sender);
                --_currentConcurrentCall;
                _autoResetEvent.Set();
            }
        }
    }
}

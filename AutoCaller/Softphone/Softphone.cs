﻿using System;
using Ozeki.VoIP;

namespace AutoCaller.Softphone
{
    public class Softphone
    {
        private readonly ISoftPhone _softphone;
        public readonly Principal Gui;
        public IPhoneLine PhoneLine;

        public Softphone(Principal gui)
        {
            Gui = gui;
            _softphone = SoftPhoneFactory.CreateSoftPhone(5700, 5750, "Blen-AutoCaller");
        }

        public void Register(
            bool registrationRequired, string displayName, string userName, 
            string registerName, string registerPassword, string domainHost, int domainPort)
        {
            try
            {
                var account = new SIPAccount(
                                        registrationRequired, 
                                        displayName, 
                                        userName,
                                        registerName, 
                                        registerPassword, 
                                        domainHost, 
                                        domainPort
                                    );
                
                PhoneLine = _softphone.CreatePhoneLine(account);
                PhoneLine.RegistrationStateChanged += phoneLine_RegistrationStateChanged;
                _softphone.RegisterPhoneLine(PhoneLine);

            }
            catch (Exception ex)
            {
                Console.WriteLine($@"Error during SIP registration: {ex}");
            }
        }

        private void phoneLine_RegistrationStateChanged(object sender, RegistrationStateChangedArgs e)
        {
            PhoneLineStateChanged?.Invoke(this, e);
        }

        public event EventHandler<RegistrationStateChangedArgs> PhoneLineStateChanged;

        public IPhoneCall CreateCall(string dial)
        {
            return _softphone.CreateCallObject(PhoneLine, dial);
        }
    }
}

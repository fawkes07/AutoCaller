﻿using System;
using System.Threading;
using System.Threading.Tasks;
using AutoCaller.Data.RegistroLlamadas;
using AutoCaller.Models;
using Ozeki.Media;
using Ozeki.VoIP;

namespace AutoCaller.Softphone
{
    public class CallHandler
    {
        private readonly Softphone _softphone;
        private readonly InfoClientCall _callInfo;
        private readonly MediaConnector _connector;
        private readonly PhoneCallAudioSender _mediaSender;
        
        private bool _isSeconTry = false;
        private bool _contestada = false;
        private string telefonoEnLlamada;
        
        public CallHandler(Softphone softphone, InfoClientCall callInfo)
        {
            _softphone = softphone;
            _callInfo = callInfo;
            _mediaSender = new PhoneCallAudioSender();
            _connector = new MediaConnector();
        }

        public event EventHandler Completed;
        
        public void Start()
        {
            if (_isSeconTry)
            {
                if (string.IsNullOrWhiteSpace(_callInfo.Cliente?.Telefono2))
                    return;
                telefonoEnLlamada = _callInfo.Cliente?.Telefono2;
            }
            else
            {
                telefonoEnLlamada = string.IsNullOrWhiteSpace(_callInfo.Cliente?.Telefono1)
                    ? _callInfo.Cliente?.Telefono2
                    : _callInfo.Cliente?.Telefono1;
            }

            _isSeconTry = true;

            var call = _softphone.CreateCall("9" + telefonoEnLlamada);
            call.CallStateChanged += OutgoingCallStateChanged;
            _mediaSender.AttachToCall(call);

            call.Start();
            _softphone.Gui.RegisterLog($"Intentando llamar al cliente {_callInfo?.Cliente?.ClienteClave} : " +
                                       $"{_callInfo?.Cliente?.Nombre} al {telefonoEnLlamada}");

            Task.Run(() =>
                {
                    ManagmentCallRegister.ChangeStatusCall(_callInfo.Cliente.ClienteClave,
                        Enums.EstatusLlamada.Pendiente);
                });

            Thread.Sleep(3 * 60 * 1000);

            if (!call.CallState.IsCallEnded())
            {
                call.HangUp();
            }
        }

        private void TextToSpeech(string text)
        {
            var textToSpeech = new TextToSpeech();
            _connector.Connect(textToSpeech, _mediaSender);
            textToSpeech.AddAndStartText(text);
        }
        
        private void OutgoingCallStateChanged(object sender, CallStateChangedArgs e)
        {
            switch (e.State)
            {
                case CallState.Created:
                case CallState.Setup:
                    _softphone.Gui.UpdateTable(_callInfo.Cliente.ClienteClave, "Intentando");
                    break;
                case CallState.Error:
                    _softphone.Gui.UpdateTable(_callInfo.Cliente.ClienteClave, "Error");
                    break;
                case CallState.Ringing:
                case CallState.RingingWithEarlyMedia:
                    _softphone.Gui.UpdateTable(_callInfo.Cliente.ClienteClave, "Timbrando");
                    break;
                case CallState.Busy:
                    _softphone.Gui.UpdateTable(_callInfo.Cliente.ClienteClave, "No contestada");
                    break;
                case CallState.Answered:
                    _contestada = true;
                    _softphone.Gui.UpdateTable(_callInfo.Cliente.ClienteClave, "Reproduciendo msj");
                    _softphone.Gui.RegisterLog($"Llamada acepta por {telefonoEnLlamada}");
                    Task.Run(() =>
                    {
                        ManagmentCallRegister.ChangeStatusCall(_callInfo.Cliente.ClienteClave,
                            Enums.EstatusLlamada.EnCurso);
                    });
                    _softphone.Gui.RegisterLog($"Reproduciendo mensaje: {_callInfo.Message}");
                    TextToSpeech(_callInfo.Message);
                    
                    Task.Run(() =>
                    {
                        ManagmentCallRegister.ChangeStatusCall(_callInfo.Cliente.ClienteClave,
                            Enums.EstatusLlamada.Contestada);
                    });
                    break;
                case CallState.Completed:
                    _softphone.Gui.UpdateTable(_callInfo.Cliente.ClienteClave, "OK");
                    break;
            }

            if (!e.State.IsCallEnded())
                return;

            _softphone.Gui.RegisterLog($"Llamada de {telefonoEnLlamada} finalizada por : {e.Reason}");

            if (!_contestada && !_isSeconTry)
            {
                Start();
            }

            Completed?.Invoke(this, EventArgs.Empty);
        }
    }
}
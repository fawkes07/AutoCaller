﻿using System;
using System.IO;
using System.Text;
using System.Windows.Forms;
using AutoCaller.Models;
using Newtonsoft.Json;

namespace AutoCaller.ConfigManagment
{
    public class ConfigFileManagment
    {
        private static readonly string FilePath = 
            Application.StartupPath + "\\config.json";

        public static LocalResponse GetConfigurationsByFile()
        {
            var settgins = new MyConfigFile();

            using (var fileStream = File.Open(
                FilePath, FileMode.OpenOrCreate, FileAccess.ReadWrite))
            {
                // Si el archivo no existia se genero en blanco asi que se
                // guardan las configuraciones por default
                if (fileStream.Length <= 0)
                {
                    return new LocalResponse(
                        "El archivo no contiene coniguracion");
                }

                using (var streamReader = new StreamReader(fileStream))
                    try
                    {
                        settgins = JsonConvert.
                            DeserializeObject<MyConfigFile>(
                                streamReader.ReadToEnd());
                    }
                    catch (Exception ex)
                    {
                        return new LocalResponse(
                            ex, "El archivo no tiene el formato adeacuado");
                    }

                return new LocalResponse() {Success = true, RObjet = settgins};
            }
        }

        public static LocalResponse SaveConffigurationFile(
            MyConfigFile settings)
        {
            try
            {
                using (var fileStream = File.Open(
                    FilePath, FileMode.OpenOrCreate, FileAccess.ReadWrite))
                {
                    var configJson = JsonConvert.SerializeObject(settings);

                    try
                    {
                        fileStream.Write(
                            Encoding.UTF8.GetBytes(configJson),
                            0, 
                            configJson.Length);
                    }
                    catch (Exception ex)
                    {
                        return new LocalResponse(
                            ex, "El rechivo no se a podido escribir");
                    }

                    return new LocalResponse(settings);
                }
            }
            catch (Exception e)
            {
                return new LocalResponse(e, e.Message);
            }
        }
    }
}

﻿using AutoCaller.Models;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace AutoCaller.Bussines.Settings
{
    public class Settgins
    {
        public static Response LoadPBXConfigurations()
        {
            var filePath = Application.StartupPath + "\\config.json";
            var configs = new Config();
            var response = new Response();

            using (var fileStream = File.Open(filePath, FileMode.OpenOrCreate, FileAccess.ReadWrite))
            {
                if (fileStream.Length > 0)
                {
                    using (var streamReader = new StreamReader(fileStream))
                    {
                        try
                        {
                            configs = JsonConvert.DeserializeObject<Config>(streamReader.ReadToEnd());
                            response.Success = true;
                            response.RObjet = configs;
                        }
                        catch (Exception e)
                        {
                            response.Exp = e;
                            response.ErrorMessage = "No se ha podido leer el archivo de configuracion!";
                            return response;
                        }
                    }
                    return response;
                }
                
                // Set defaul values and create confi.file

                configs.RegistrationRequired = true;
                configs.AuthenticationId = string.Empty;
                configs.DisplayName = "610";
                configs.UserName = "610";
                configs.RegisterName = "610";
                configs.RegisterPassword = "V7fXE7Q2mCFAvyG";
                configs.DomainHost = "192.168.1.45";
                configs.DomainPort = 5056;

                response.RObjet = configs;
                response.Success = true;

                var configJson = JsonConvert.SerializeObject(configs);

                try
                {
                    fileStream.Write(Encoding.UTF8.GetBytes(configJson), 0, configJson.Length);
                }
                catch (Exception e)
                {
                    response.Exp = e;
                    response.ErrorMessage = "No se ha podido escribir en archivo de configuracion las configuraciones default";
                    return response;
                }

                return response;

            }
        }
    }
}

﻿using AutoCaller.ConfigManagment;
using AutoCaller.Data.RegistroLlamadas;
using AutoCaller.Models;
using AutoCaller.Reporting;
using AutoCaller.Softphone;
using AutoCaller.Views;
using Ozeki.VoIP;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AutoCaller
{
    public partial class Principal : Form
    {
        #region campos

        //Constantes
        private static readonly int MaxConcurrentCall = 1;

        //Globales
        private MyConfigFile _pbxSettgins;

        // Flags
        public bool ProcesandoLlamadas;
        public bool IsCorrectConfig;
        public bool IsAutomaticMode;
        public bool IsRuningAutomatic;

        public DateTime AgendaSiguienteCiclo { get; set; }

        //Softphone
        private Softphone.Softphone _mySoftphone;
        private RegState _phoneLineInformation;
        private readonly List<InfoClientCall> _listCall;

        private Autodialer _autodialer;

        //GUI
        private readonly List<DgvClientesModel> _clientesTab;

        #endregion

        #region Ctor

        public Principal()
        {
            // Inicializaciones
            InitializeComponent();
            ProcesandoLlamadas = false;
            IsCorrectConfig = false;
            IsAutomaticMode = false;
            IsRuningAutomatic = false;

            _clientesTab = new List<DgvClientesModel>();
            _listCall = new List<InfoClientCall>();
            
            // Carga de configuracion segun archivo
            var result = ConfigFileManagment.GetConfigurationsByFile();

            if(result.Success)
                _pbxSettgins = result.RObjet as MyConfigFile;
            else
            {
                var messageResult = MessageBox.Show(
                    @"No se han podido cargar los valores anteriores, " + 
                    @"\npor favor configure su conexion al conmutador",
                    @"Error", MessageBoxButtons.OKCancel, MessageBoxIcon.Error);

                switch (messageResult)
                {
                    case DialogResult.OK:
                        new Configuracion().ShowDialog();

                        result = ConfigFileManagment.GetConfigurationsByFile();
                        if (result.Success)
                            _pbxSettgins = result.RObjet as MyConfigFile;
                        else
                            Dispose();
                        break;
                    case DialogResult.None:
                    case DialogResult.Cancel:
                    case DialogResult.Abort:
                    case DialogResult.Retry:
                    case DialogResult.Ignore:
                    case DialogResult.Yes:
                    case DialogResult.No:
                        Dispose();
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }

        #endregion

        #region metodos del softphone
        
        private void InitSoftphone()
        {
            _mySoftphone = new Softphone.Softphone(this);
            _mySoftphone.PhoneLineStateChanged += phoneLine_PhoneLineInformation;

            RegisterLog("Softphone Creado!");

            _mySoftphone.Register(
                _pbxSettgins.RegistrationRequired,
                _pbxSettgins.DisplayName,
                _pbxSettgins.UserName,
                _pbxSettgins.RegisterName,
                _pbxSettgins.RegisterPassword,
                _pbxSettgins.DomainHost,
                _pbxSettgins.DomainPort
                );

            RegisterLog("Linea telefonica Creada!");
        }

        // Cambios de estatus en la linea
        private void phoneLine_PhoneLineInformation(object sender, RegistrationStateChangedArgs e)
        {
            _phoneLineInformation = e.State;
            var messageToLog = string.Empty;

            switch (_phoneLineInformation)
            {
                case RegState.RegistrationSucceeded:
                    messageToLog = "Registration succeeded - Online";
                    break;
                case RegState.Error:
                    messageToLog = $"Not registered - Offline: {_phoneLineInformation}";

                    var respuesta = MessageBox.Show(
                        @"Imposible conectar con el servidor de VoIP",
                        @"Error",
                        MessageBoxButtons.RetryCancel,
                        MessageBoxIcon.Error);

                    switch (respuesta)
                    {
                        case DialogResult.Retry:
                            Task.Run(() => InitSoftphone());
                            break;
                        case DialogResult.Cancel:
                            InvokeGuiThread(() =>
                            {
                                btn_configPhone.Enabled = true;
                                btnRegistrar.Enabled = true;
                            });
                            AbrirYCargarConfigs();
                            break;
                    }

                    break;
                case RegState.UnregRequested:
                    messageToLog = "Registration UnregRequested";
                    break;
                case RegState.NotRegistered:
                    messageToLog = "Softfphone Not Registered ";
                    break;
                case RegState.RegistrationRequested:
                    messageToLog = "Softfphone: RegistrationRequested ";
                    break;
            }

            if (!string.IsNullOrWhiteSpace(messageToLog))
                RegisterLog(messageToLog);

            InvokeGuiThread(() =>
            {
                lblState.Text = _phoneLineInformation == RegState.RegistrationSucceeded ? "ONLINE" : "OFFLINE";
            });

            ValidarYActivarBntStart();
        }

        #endregion

        #region Metodos de conexion a componentes de la vista
        
        private void InvokeGuiThread(Action action)
        {
            Invoke(action);
        }

        public void RegisterLog(string logMessage)
        {
            InvokeGuiThread(() =>
            {
                Lb_Log.Items.Add($"{DateTime.Now:g} \t{logMessage}");
            });
        }

        public void UpdateTable(string id, string nuevoEstatus)
        {
            InvokeGuiThread(() =>
            {
                var cliente = _clientesTab.FirstOrDefault(clte => clte.ClienteClave == id);

                if (cliente != null)
                {
                    cliente.LlamadaEstatus = nuevoEstatus;
                }
                DgvClientes.Refresh();

                DgvClientes.DataSource = _clientesTab;
            });
        }

        #endregion
        
        // Trae de la base de datos a los clientes y los ingresa a un dataset (lista)
        private void CargarClientes()
        {
            InvokeGuiThread(() =>
            {
                _clientesTab.Clear();
                DgvClientes.DataSource = null;
                PbClientes.Visible = true;
                DgvClientes.Refresh();
                PbClientes.Text = @"Cargando clientes";
            });
            
            var response = ManagmentCallRegister.GetCallsToDo();
            
            if (!response.Success)
            {
                MessageBox.Show(
                        $@"Error al consultar la base de datos: {response.Exception?.Message ?? response.ErrorMessage}",
                        @"Error",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Error
                    );
                return;
            }
            
            var dt = response.RObjet as DataTable;
            
            foreach (var cliente in dt.AsEnumerable())
            {
                var tel1 = Regex.Replace(cliente["TELEFONO1"].ToString() ?? "", "[^0-9]", "", RegexOptions.None);
                var tel2 = Regex.Replace(cliente["TELEFONO2"].ToString() ?? "", "[^0-9]", "", RegexOptions.None);

                if (tel1.Length > 10)
                    tel1 = tel1.Substring(tel1.Length - 10);

                if (tel2.Length > 10)
                    tel2 = tel2.Substring(tel2.Length - 10);

                tel1 = string.IsNullOrWhiteSpace(tel1) ? "" : tel1.PadLeft(10, '3');
                tel2 = string.IsNullOrWhiteSpace(tel2) ? "" : tel2.PadLeft(10, '3');

                decimal saldo;
                decimal.TryParse(cliente["SALDO"].ToString() ?? "", out saldo);

                //ManagmentCallRegister.ChangeStatusCall(cliente["CLAVE_PROMOTOR"].ToString(), Enums.EstatusLlamada.Pendiente, this);

                var llamada = new DgvClientesModel(
                    cliente["CLAVE_PROMOTOR"].ToString(),
                    cliente["PROMOTOR"].ToString(),
                    tel1, tel2,
                    saldo
                );

                llamada.LlamadaEstatus = "En cola";

                _clientesTab.Add(llamada);
            }
#if DEBUG
            RegisterLog("fin del procesamiento y generacion de tabla");
#endif

            InvokeGuiThread(() =>
            {
                btn_UpdateTabla.Visible = true;
                PbClientes.Visible = false;
                DgvClientes.DataSource = _clientesTab; 
                DgvClientes.Refresh();
                lbl_countRegistros.Text = _clientesTab.Count.ToString();
            });

            RegisterLog($"Se cargaron {DgvClientes.RowCount} registros");

            ValidarYActivarBntStart();
        }

        // Genera los mensajes que le sera reproducido al los clientes
        private void GenerarSpeachs()
        {
            foreach (var cliente in _clientesTab)
            {
                _listCall.Add( new InfoClientCall
                {
                    Cliente = cliente,
                    Message = TbxMessage.Text
                                    .Replace("[cliente.nombre]", cliente.Nombre)
                                    .Replace("[cliente.saldo]", cliente.SaldoRequerido.ToString(CultureInfo.InvariantCulture))
                });
            }
        }

        // Valida confiuracion y permite iniciar con las llamadas
        private void ValidarYActivarBntStart()
        {
            if (_clientesTab.Any() &&
                TbxMessage.Text != string.Empty &&
                _mySoftphone?.PhoneLine?.RegState != null &&
                _mySoftphone?.PhoneLine?.RegState == RegState.RegistrationSucceeded)
            {
                IsCorrectConfig = true;
                InvokeGuiThread(() =>
                {
                    BtnStart.Enabled = IsCorrectConfig;
                });
            }
            else
            {
                IsCorrectConfig = false;

                if (IsRuningAutomatic)
                {
                    RegisterLog("La configuracion se modifico de forma invalida, \n" +
                                "No se podra iniciar el siguiente ciclo de llamadas");
                }

                InvokeGuiThread(() =>
                {
                    BtnStart.Enabled = IsCorrectConfig;
                });
            }
        }

        // abre la ventana de configuracion del PBX para hacer una reconfiguracion
        private void AbrirYCargarConfigs()
        {
            InvokeGuiThread(() =>
            {
                new Configuracion().ShowDialog();
            });

            var result = ConfigFileManagment.GetConfigurationsByFile();

            if (result.Success)
            {
                _pbxSettgins = result.RObjet as MyConfigFile;

                InvokeGuiThread(() =>
                {
                    LblDisplayName.Text = _pbxSettgins.DisplayName;
                    lblHost.Text = $@"{_pbxSettgins.DomainHost}:{_pbxSettgins.DomainPort}";
                    lblState.Text = @"No registrado";
                });
            }
            else
                MessageBox.Show(
                    @"No se pudo cargar la configuracion debido a que\n\n" +
                    result.ErrorMessage + "\n\nReintente configurar su conexión",
                    @"Error",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
        }

        public void IsRuning(bool isRuning, bool isAutomaticProcess = false)
        {
            IsRuningAutomatic = isAutomaticProcess;

            InvokeGuiThread(() =>
            {
                btn_Cancel.Enabled = isRuning;
                BtnStart.Visible = !isRuning;
                gbLlamadasEnProgreso.Visible = isRuning;
                TbxMessage.Enabled = !isRuning;
                btn_UpdateTabla.Enabled = !isRuning;
            });
        }

        // Funcion de camio de estado de llamadas
        public void LlamadasEnProgreso(bool estaEnProgreso)
        {
            ProcesandoLlamadas = estaEnProgreso;
            
            if (ProcesandoLlamadas)
            {
                return;
            }

            // Ya no hay llamadas procesandose
            RegisterLog("===== Cola de llamadas finalizada.");
            if (IsAutomaticMode)
            {
                // Si esta en modo automatico debe
                //  - No Guardar el log hasta finalizado el dia (despues de las 7 PM)
                //  - No debe desbloquear los controles de llamadas
                //  - Al terminar la lista de llamadas debe agendar el siguiente inicio
                //      tomando en cuenta que si se termino y 1 hr despues es hora valida de llamadas
                //      debe hacer otro ciclo de llamadas en caso contrairio la agenda se pasa al siguiente dia
                //  - Al terminar debe recargar la lista de llamadas a realizar

                AgendaSiguienteCiclo = 
                    DateTime.Now.AddHours(1).TimeOfDay < endTime.Value.TimeOfDay ? 
                        DateTime.Now.AddHours(1) : 
                        DateTime.Now.AddDays(1).Add(startTime.Value.TimeOfDay);
                RegisterLog($"Se agenda siguiente inicio para: {AgendaSiguienteCiclo:g}");
            }
            else
            {
                // En modo manual debe
                //  - Al terminar el proceso de llamadas genera el reporte
                //  - Se detiene

                RegisterLog("Esperando finalicen todas las llamadas...");

                // Se espera 3 min para que se finalicen las llamadas
                // TODO: Mecaniscmo para conocer cuando todas las llamadas taerminaron 
                Thread.Sleep(3 * 60 * 1000);
                RegisterLog("Tiempo estimado finalizado");

                GuardarLog();

                IsRuning(false);
            }
        }

        // Genera un reporte a partir del log y lo guarda en un archivo
        public void GuardarLog()
        {
            var logs = new string[] { };

            InvokeGuiThread(() =>
            {
                logs = Lb_Log.Items.OfType<string>().ToArray();
            });

            if (logs.Any())
                Task.Run(() => GenerateLogFile.SaveLogInAFile(logs, 0, 0));

            InvokeGuiThread(() =>
            {
                Lb_Log.Items.Clear();
            });
        }
        
        #region Eventos de la ventana

        private void Principal_Shown(object sender, EventArgs e)
        {
            InvokeGuiThread(() =>
            {
                LblDisplayName.Text = _pbxSettgins.DisplayName;
                lblHost.Text = $@"{_pbxSettgins.DomainHost}:{_pbxSettgins.DomainPort}";
                lblState.Text = @"No registrado";
            });

            Task.Run(() => CargarClientes());
        }

        private void BtnStart_Click(object sender, EventArgs e)
        {
            if (IsAutomaticMode)
            {
                IsRuning(true, true);
                RegisterLog("Llamadas en modo automatico iniciado ");
            }
            else
            {
                IsRuning(true);

                // Se inicia el proceso de llamadas

                LlamadasEnProgreso(true);

                // modo manual
                RegisterLog("Llamadas en modo manual iniciado...");
                GenerarSpeachs();
                _autodialer = new Autodialer(_mySoftphone, _listCall, MaxConcurrentCall);
                _autodialer.Start();
            }
        }

        private void TbxMessage_TextChanged(object sender, EventArgs e)
        {
            ValidarYActivarBntStart();
        }
        
        private void btnRegistrar_Click(object sender, EventArgs e)
        {
            InvokeGuiThread(() =>
            {
                btnRegistrar.Enabled = false;
                btn_configPhone.Enabled = false;
            });

            Task.Run(() => InitSoftphone());
        }

        private void btn_Cancel_Click(object sender, EventArgs e)
        {
            RegisterLog("Se a iniciado el proceso de fin de llamadas. Espere...");
            _autodialer?.Stop();
            btn_Cancel.Enabled = false;
            Task.Run(() =>
            {
                Thread.Sleep(3 * 60 * 1000);
                GuardarLog();
            });
        }

        private void btn_configPhone_Click(object sender, EventArgs e)
        {
            AbrirYCargarConfigs();
        }
        
        private void IsAutomatic_CheckedChanged(object sender, EventArgs e)
        {
            IsAutomaticMode = IsAutomatic.Checked;

            startTime.Enabled = IsAutomaticMode;
            endTime.Enabled = IsAutomaticMode;

            if (IsAutomaticMode)
            {
                var now = DateTime.Now;
                AgendaSiguienteCiclo = new DateTime(
                    now.Year, 
                    now.Month, 
                    now.Day, startTime.Value.Hour, startTime.Value.Minute, 0);

                dtpAgenda.Value = AgendaSiguienteCiclo;
            }
        }

        private void btn_UpdateTabla_Click(object sender, EventArgs e)
        {
            Task.Run(() => CargarClientes());
        }
        
        private void startTime_ValueChanged(object sender, EventArgs e)
        {
            var current = dtpAgenda.Value;
            AgendaSiguienteCiclo = new DateTime(
                current.Year,
                current.Month,
                current.Day, startTime.Value.Hour, startTime.Value.Minute, 0);

            dtpAgenda.Value = AgendaSiguienteCiclo;
        }

        #endregion

        // Automatizaciones
        private async void timerSchedule_Tick(object sender, EventArgs e)
        {
            if (!IsCorrectConfig || !IsAutomaticMode || !IsRuningAutomatic)
                return;
            
            var now = DateTime.Now;

            // Validacion de inicio de proceso
            // No se deben estar procesando llamadas y la fecha debe ser valida
            if (!ProcesandoLlamadas && now >= AgendaSiguienteCiclo)
            {
#if DEBUG
                RegisterLog("proceso automatico inicia");
#endif

                // iniciar proceso
                LlamadasEnProgreso(true);

                CargarClientes();

                // No se recuperan registros por lo que se reagenda el inicio
                if (DgvClientes.RowCount <= 0)
                {
                    RegisterLog("No hay llamadas a realizar");
                    ReagendarInicio();
                    LlamadasEnProgreso(false);
                    return;
                }

                RegisterLog($"Se recuperaron {DgvClientes.RowCount} registros");

                GenerarSpeachs();

                RegisterLog("Se han generado los speach");
                _autodialer = new Autodialer(_mySoftphone, _listCall, MaxConcurrentCall);
                RegisterLog("Inicia ciclo de llamadas");
                _autodialer.Start();
            }

            // Validacion de fin de proceso
            if (ProcesandoLlamadas && now.TimeOfDay > endTime.Value.TimeOfDay)
            {
                // terminar cola de llamadas
                _autodialer.Stop();
                RegisterLog("Las llamadas terminaron por configuracion de hora");
                ReagendarInicio();
                LlamadasEnProgreso(false);

                // despues de 10 minutos se guarda el reporte
                await Task.Run(() =>
                {
                    RegisterLog("Espere 3 min para que se guarde el registro.");
                    Thread.Sleep(3 * 60 * 1000);
                    GuardarLog();
                });
            }
        }
        
        private void ReagendarInicio()
        {
            var now = DateTime.Now;

            if (DateTime.Now.AddHours(1).TimeOfDay < endTime.Value.TimeOfDay)
            {
                AgendaSiguienteCiclo = DateTime.Now.AddHours(1);
            }
            else
            {
                now = now.AddDays(1);
                AgendaSiguienteCiclo = new DateTime(
                    now.Year, now.Month, now.Day,
                    startTime.Value.Hour, startTime.Value.Minute, 0);
            }

            dtpAgenda.Value = AgendaSiguienteCiclo;

            RegisterLog($"Se agenda el nuevo ciclo a iniciar el: {AgendaSiguienteCiclo:g}");
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            var helper = new HelpCadenas();
            helper.ShowDialog(this);
        }

        private void Principal_Load(object sender, EventArgs e)
        {
#if DEBUG
            TbxMessage.Text = ". \n " +
                              "Este es un mensaje de prueba del sistema de llamadas automaticas de blen";
#endif
        }
    }
}
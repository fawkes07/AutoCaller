﻿using System;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace AutoCaller.Reporting
{
    public class GenerateLogFile
    {
        private static string _logFilePath =
            Application.StartupPath + "\\Log\\";

        public static void SaveLogInAFile(string[] logs, int totalCalls, int answeredCalls)
        {
            if (!Directory.Exists(_logFilePath))
            {
                Directory.CreateDirectory(_logFilePath);
            }

            var s = DateTime.Now.ToString("ddMMyyyyHHmm");
            _logFilePath = _logFilePath + "Log_" + s;
            
            #region Generacion de registro

            var stream = new StringBuilder();

            stream.Append("======================================================================\n");
            stream.Append($"                        REGISTRO DEL DIA {s}\n");
            stream.Append("======================================================================\n");
            stream.Append("\n\n\n");

            foreach (var log in logs)
            {
                stream.Append(log + "\n");
            }

            stream.Append("\n\n\n");
            stream.Append("======================================================================\n");
            stream.Append("                          REPORTE DE LLAMADAS\n");
            stream.Append("======================================================================\n");
            stream.Append("\n\n");
            stream.Append("Llamadas realizadas: " + totalCalls);
            stream.Append("Llamadas contestadas: " + answeredCalls);

            #endregion

            using (var fileStream = File.Open(
                _logFilePath, FileMode.OpenOrCreate, FileAccess.ReadWrite))
            {
                try
                {
                    fileStream.Write(
                        Encoding.UTF8.GetBytes(stream.ToString()),
                        0,
                        stream.Length);
                }
                catch (Exception ex)
                {
                    Console.Write("VALIO MADRE LA GENERACION DEL LOG POR : " + ex.Message);
                }
            }
        }
    }
}

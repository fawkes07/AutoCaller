﻿using System;
using Ozeki.Media;
using Ozeki.VoIP;

namespace AutoCaller.SoftphoneDialer
{
    public class CallHandler
    {
        readonly ISoftPhone _softphone;
        private readonly IPhoneLine _phoneLine;
        readonly CallInfo _callInfo;
        readonly MediaConnector _connector;
        readonly PhoneCallAudioSender _mediaSender;
        
        public CallHandler(ISoftPhone softphone, IPhoneLine phoneLine,CallInfo callInfo)
        {
            _softphone = softphone;
            _phoneLine = phoneLine;
            _callInfo = callInfo;
            _mediaSender = new PhoneCallAudioSender();
            _connector = new MediaConnector();
        }

        public event EventHandler Completed;

        public void Start()
        {
            var call = _softphone.CreateCallObject(_phoneLine, _callInfo.PhoneNumber);
            call.CallStateChanged += OutgoingCallStateChanged;
            _mediaSender.AttachToCall(call);
            call.Start();
            Console.WriteLine($"Trying to call: {_callInfo.PhoneNumber}.");
        }
        
        void TextToSpeech(string text)
        {
            var textToSpeech = new TextToSpeech();
            _connector.Connect(textToSpeech, _mediaSender);
            textToSpeech.AddAndStartText(text);
        }
        
        private void OutgoingCallStateChanged(object sender, CallStateChangedArgs e)
        {
            if (e.State == CallState.Answered)
            {
                Console.WriteLine("\nCall has been accepted by {0}.", _callInfo.PhoneNumber);
                TextToSpeech(_callInfo.Message);
                Console.WriteLine("Playing message: \n\"{0}\"", _callInfo.Message);
            }
            else if (e.State.IsCallEnded())
            {
                Console.WriteLine("\nCall has been ended: {0}.", _callInfo.PhoneNumber);
                var handler = Completed;
                if (handler != null)
                    handler(this, EventArgs.Empty);
            }
        }
    }
}

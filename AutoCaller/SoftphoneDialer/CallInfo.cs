﻿namespace AutoCaller.SoftphoneDialer
{
    public class CallInfo
    {
        public string PhoneNumber { get; private set; }

        public string Message { get; private set; }

        public CallInfo(string phoneNumber, string message)
        {
            PhoneNumber = phoneNumber;
            Message = message;
        }
    }
}

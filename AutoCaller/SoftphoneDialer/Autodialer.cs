﻿using Ozeki.VoIP;
using System;
using System.Collections.Generic;
using System.Threading;

namespace AutoCaller.SoftphoneDialer
{
    public class Autodialer
    {
        readonly ISoftPhone _softphone;
        private IPhoneLine _phoneLine;
        readonly List<CallInfo> _callList;
        readonly int _maxConcurrentCall;
        int _currentConcurrentCall;
        readonly List<CallHandler> _callHandlers;
        readonly AutoResetEvent _autoResetEvent;
        readonly object _sync;

        public Autodialer(ISoftPhone softphone, IPhoneLine phoneLine, List<CallInfo> callList, int maxConcurrentCall)
        {
            _sync = new object();
            _softphone = softphone;
            _phoneLine = phoneLine;
            _callList = callList;
            _maxConcurrentCall = maxConcurrentCall;
            _callHandlers = new List<CallHandler>();
            _autoResetEvent = new AutoResetEvent(false);
        }

        public void Start()
        {
            ThreadPool.QueueUserWorkItem(o =>
            {
                foreach (var callInfo in _callList)
                {
                    if (_currentConcurrentCall < _maxConcurrentCall)
                    {
                        StartCallHandler(callInfo);
                    }
                    else
                    {
                        _autoResetEvent.WaitOne();
                        StartCallHandler(callInfo);
                    }
                }
            });
        }

        void StartCallHandler(CallInfo callInfo)
        {
            lock (_sync)
            {
                ++_currentConcurrentCall;
                var callHandler = new CallHandler(_softphone, _phoneLine, callInfo);
                callHandler.Completed += CallHandler_Completed;
                _callHandlers.Add(callHandler);

                callHandler.Start();
            }
        }

        void CallHandler_Completed(object sender, EventArgs e)
        {
            lock (_sync)
            {
                _callHandlers.Remove((CallHandler)sender);
                --_currentConcurrentCall;
                _autoResetEvent.Set();
            }
        }
    }
}

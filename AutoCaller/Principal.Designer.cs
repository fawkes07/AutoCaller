﻿namespace AutoCaller
{
    partial class Principal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Principal));
            this.Lb_Log = new System.Windows.Forms.ListBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btn_configPhone = new System.Windows.Forms.Button();
            this.btnRegistrar = new System.Windows.Forms.Button();
            this.lblState = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lblHost = new System.Windows.Forms.Label();
            this.LblDisplayName = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.DgvClientes = new System.Windows.Forms.DataGridView();
            this.TbxMessage = new System.Windows.Forms.TextBox();
            this.BtnStart = new System.Windows.Forms.Button();
            this.lblNota = new System.Windows.Forms.Label();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.PbClientes = new System.Windows.Forms.ProgressBar();
            this.gbLlamadasEnProgreso = new System.Windows.Forms.GroupBox();
            this.btn_Cancel = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.pbLlamadas = new System.Windows.Forms.ProgressBar();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.dtpAgenda = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.linkLabel2 = new System.Windows.Forms.LinkLabel();
            this.IsAutomatic = new System.Windows.Forms.CheckBox();
            this.label7 = new System.Windows.Forms.Label();
            this.endTime = new System.Windows.Forms.DateTimePicker();
            this.label6 = new System.Windows.Forms.Label();
            this.startTime = new System.Windows.Forms.DateTimePicker();
            this.lbl_countRegistros = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.timerSchedule = new System.Windows.Forms.Timer(this.components);
            this.btn_UpdateTabla = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DgvClientes)).BeginInit();
            this.gbLlamadasEnProgreso.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // Lb_Log
            // 
            this.Lb_Log.CausesValidation = false;
            this.Lb_Log.FormattingEnabled = true;
            this.Lb_Log.HorizontalScrollbar = true;
            this.Lb_Log.Location = new System.Drawing.Point(12, 92);
            this.Lb_Log.Name = "Lb_Log";
            this.Lb_Log.Size = new System.Drawing.Size(313, 537);
            this.Lb_Log.TabIndex = 1;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btn_configPhone);
            this.groupBox1.Controls.Add(this.btnRegistrar);
            this.groupBox1.Controls.Add(this.lblState);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.lblHost);
            this.groupBox1.Controls.Add(this.LblDisplayName);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(398, 75);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Configuraciones PBX";
            // 
            // btn_configPhone
            // 
            this.btn_configPhone.Image = ((System.Drawing.Image)(resources.GetObject("btn_configPhone.Image")));
            this.btn_configPhone.Location = new System.Drawing.Point(248, 9);
            this.btn_configPhone.Name = "btn_configPhone";
            this.btn_configPhone.Size = new System.Drawing.Size(37, 36);
            this.btn_configPhone.TabIndex = 8;
            this.btn_configPhone.UseVisualStyleBackColor = true;
            this.btn_configPhone.Click += new System.EventHandler(this.btn_configPhone_Click);
            // 
            // btnRegistrar
            // 
            this.btnRegistrar.Location = new System.Drawing.Point(291, 9);
            this.btnRegistrar.Name = "btnRegistrar";
            this.btnRegistrar.Size = new System.Drawing.Size(96, 36);
            this.btnRegistrar.TabIndex = 7;
            this.btnRegistrar.Text = "Registrar";
            this.btnRegistrar.UseVisualStyleBackColor = true;
            this.btnRegistrar.Click += new System.EventHandler(this.btnRegistrar_Click);
            // 
            // lblState
            // 
            this.lblState.AutoSize = true;
            this.lblState.Location = new System.Drawing.Point(288, 51);
            this.lblState.Name = "lblState";
            this.lblState.Size = new System.Drawing.Size(70, 13);
            this.lblState.TabIndex = 6;
            this.lblState.Text = "No registrado";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(247, 51);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(43, 13);
            this.label5.TabIndex = 5;
            this.label5.Text = "Estado:";
            // 
            // lblHost
            // 
            this.lblHost.AutoSize = true;
            this.lblHost.Location = new System.Drawing.Point(103, 50);
            this.lblHost.Name = "lblHost";
            this.lblHost.Size = new System.Drawing.Size(127, 13);
            this.lblHost.TabIndex = 4;
            this.lblHost.Text = "<000.000.000.000:0000>";
            // 
            // LblDisplayName
            // 
            this.LblDisplayName.AutoSize = true;
            this.LblDisplayName.Location = new System.Drawing.Point(131, 27);
            this.LblDisplayName.Name = "LblDisplayName";
            this.LblDisplayName.Size = new System.Drawing.Size(56, 13);
            this.LblDisplayName.TabIndex = 2;
            this.LblDisplayName.Text = "<Nombre>";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 50);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(91, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Puerta de enlace:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 27);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(119, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Nombre de la conexion:";
            // 
            // DgvClientes
            // 
            this.DgvClientes.AllowUserToAddRows = false;
            this.DgvClientes.AllowUserToResizeRows = false;
            this.DgvClientes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DgvClientes.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.DgvClientes.EnableHeadersVisualStyles = false;
            this.DgvClientes.Location = new System.Drawing.Point(331, 93);
            this.DgvClientes.MultiSelect = false;
            this.DgvClientes.Name = "DgvClientes";
            this.DgvClientes.ReadOnly = true;
            this.DgvClientes.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DgvClientes.ShowCellErrors = false;
            this.DgvClientes.ShowCellToolTips = false;
            this.DgvClientes.ShowEditingIcon = false;
            this.DgvClientes.ShowRowErrors = false;
            this.DgvClientes.Size = new System.Drawing.Size(663, 322);
            this.DgvClientes.TabIndex = 3;
            // 
            // TbxMessage
            // 
            this.TbxMessage.AcceptsReturn = true;
            this.TbxMessage.AcceptsTab = true;
            this.TbxMessage.Location = new System.Drawing.Point(331, 445);
            this.TbxMessage.Multiline = true;
            this.TbxMessage.Name = "TbxMessage";
            this.TbxMessage.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this.TbxMessage.Size = new System.Drawing.Size(665, 102);
            this.TbxMessage.TabIndex = 4;
            this.TbxMessage.TextChanged += new System.EventHandler(this.TbxMessage_TextChanged);
            // 
            // BtnStart
            // 
            this.BtnStart.Enabled = false;
            this.BtnStart.Location = new System.Drawing.Point(331, 567);
            this.BtnStart.Name = "BtnStart";
            this.BtnStart.Size = new System.Drawing.Size(622, 63);
            this.BtnStart.TabIndex = 5;
            this.BtnStart.Text = "Comenzar llamadas";
            this.BtnStart.UseVisualStyleBackColor = true;
            this.BtnStart.Click += new System.EventHandler(this.BtnStart_Click);
            // 
            // lblNota
            // 
            this.lblNota.AutoSize = true;
            this.lblNota.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNota.Location = new System.Drawing.Point(637, 550);
            this.lblNota.Name = "lblNota";
            this.lblNota.Size = new System.Drawing.Size(343, 13);
            this.lblNota.TabIndex = 6;
            this.lblNota.Text = "*Dentro de este campo de texto puede usar tags de sustitucion de texto";
            // 
            // linkLabel1
            // 
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.linkLabel1.Location = new System.Drawing.Point(977, 550);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(13, 13);
            this.linkLabel1.TabIndex = 7;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "?";
            this.linkLabel1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
            // 
            // PbClientes
            // 
            this.PbClientes.Location = new System.Drawing.Point(573, 241);
            this.PbClientes.Name = "PbClientes";
            this.PbClientes.Size = new System.Drawing.Size(187, 15);
            this.PbClientes.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
            this.PbClientes.TabIndex = 8;
            // 
            // gbLlamadasEnProgreso
            // 
            this.gbLlamadasEnProgreso.Controls.Add(this.btn_Cancel);
            this.gbLlamadasEnProgreso.Controls.Add(this.label1);
            this.gbLlamadasEnProgreso.Controls.Add(this.pbLlamadas);
            this.gbLlamadasEnProgreso.Location = new System.Drawing.Point(331, 566);
            this.gbLlamadasEnProgreso.Name = "gbLlamadasEnProgreso";
            this.gbLlamadasEnProgreso.Size = new System.Drawing.Size(669, 63);
            this.gbLlamadasEnProgreso.TabIndex = 9;
            this.gbLlamadasEnProgreso.TabStop = false;
            this.gbLlamadasEnProgreso.Visible = false;
            // 
            // btn_Cancel
            // 
            this.btn_Cancel.Location = new System.Drawing.Point(540, 11);
            this.btn_Cancel.Name = "btn_Cancel";
            this.btn_Cancel.Size = new System.Drawing.Size(123, 46);
            this.btn_Cancel.TabIndex = 2;
            this.btn_Cancel.Text = "Detener cola de llamadas";
            this.btn_Cancel.UseVisualStyleBackColor = true;
            this.btn_Cancel.Click += new System.EventHandler(this.btn_Cancel_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(231, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(104, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Realizando llamadas";
            // 
            // pbLlamadas
            // 
            this.pbLlamadas.Location = new System.Drawing.Point(6, 35);
            this.pbLlamadas.Name = "pbLlamadas";
            this.pbLlamadas.Size = new System.Drawing.Size(528, 22);
            this.pbLlamadas.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
            this.pbLlamadas.TabIndex = 0;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.dtpAgenda);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.linkLabel2);
            this.groupBox2.Controls.Add(this.IsAutomatic);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.endTime);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.startTime);
            this.groupBox2.Location = new System.Drawing.Point(620, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(372, 75);
            this.groupBox2.TabIndex = 9;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Automatizacion Horas";
            // 
            // dtpAgenda
            // 
            this.dtpAgenda.CustomFormat = "dd/MM/yyyy HH:mm";
            this.dtpAgenda.Enabled = false;
            this.dtpAgenda.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpAgenda.Location = new System.Drawing.Point(237, 15);
            this.dtpAgenda.Name = "dtpAgenda";
            this.dtpAgenda.Size = new System.Drawing.Size(127, 20);
            this.dtpAgenda.TabIndex = 14;
            this.dtpAgenda.Value = new System.DateTime(2009, 1, 4, 23, 30, 0, 0);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(133, 19);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(108, 13);
            this.label4.TabIndex = 13;
            this.label4.Text = "Inicio siguiente ciclo: ";
            // 
            // linkLabel2
            // 
            this.linkLabel2.AutoSize = true;
            this.linkLabel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.linkLabel2.Location = new System.Drawing.Point(114, 20);
            this.linkLabel2.Name = "linkLabel2";
            this.linkLabel2.Size = new System.Drawing.Size(13, 13);
            this.linkLabel2.TabIndex = 12;
            this.linkLabel2.TabStop = true;
            this.linkLabel2.Text = "?";
            // 
            // IsAutomatic
            // 
            this.IsAutomatic.AutoSize = true;
            this.IsAutomatic.Location = new System.Drawing.Point(9, 19);
            this.IsAutomatic.Name = "IsAutomatic";
            this.IsAutomatic.Size = new System.Drawing.Size(109, 17);
            this.IsAutomatic.TabIndex = 11;
            this.IsAutomatic.Text = "Modo Automatico";
            this.IsAutomatic.UseVisualStyleBackColor = true;
            this.IsAutomatic.CheckedChanged += new System.EventHandler(this.IsAutomatic_CheckedChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(218, 51);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(75, 13);
            this.label7.TabIndex = 9;
            this.label7.Text = "Ultima llamada";
            // 
            // endTime
            // 
            this.endTime.CustomFormat = "HH:mm";
            this.endTime.Enabled = false;
            this.endTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.endTime.Location = new System.Drawing.Point(299, 46);
            this.endTime.Name = "endTime";
            this.endTime.ShowUpDown = true;
            this.endTime.Size = new System.Drawing.Size(57, 20);
            this.endTime.TabIndex = 10;
            this.endTime.Value = new System.DateTime(1991, 10, 22, 19, 0, 0, 0);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(5, 51);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(75, 13);
            this.label6.TabIndex = 8;
            this.label6.Text = "Primer llamada";
            // 
            // startTime
            // 
            this.startTime.CustomFormat = "HH:mm";
            this.startTime.Enabled = false;
            this.startTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.startTime.Location = new System.Drawing.Point(86, 46);
            this.startTime.Name = "startTime";
            this.startTime.ShowUpDown = true;
            this.startTime.Size = new System.Drawing.Size(54, 20);
            this.startTime.TabIndex = 8;
            this.startTime.Value = new System.DateTime(1990, 3, 29, 11, 0, 0, 0);
            this.startTime.ValueChanged += new System.EventHandler(this.startTime_ValueChanged);
            // 
            // lbl_countRegistros
            // 
            this.lbl_countRegistros.AutoSize = true;
            this.lbl_countRegistros.Location = new System.Drawing.Point(391, 425);
            this.lbl_countRegistros.Name = "lbl_countRegistros";
            this.lbl_countRegistros.Size = new System.Drawing.Size(31, 13);
            this.lbl_countRegistros.TabIndex = 10;
            this.lbl_countRegistros.Text = "0000";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(331, 425);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(54, 13);
            this.label8.TabIndex = 11;
            this.label8.Text = "Registros:";
            // 
            // timerSchedule
            // 
            this.timerSchedule.Enabled = true;
            this.timerSchedule.Interval = 60000;
            this.timerSchedule.Tick += new System.EventHandler(this.timerSchedule_Tick);
            // 
            // btn_UpdateTabla
            // 
            this.btn_UpdateTabla.Image = ((System.Drawing.Image)(resources.GetObject("btn_UpdateTabla.Image")));
            this.btn_UpdateTabla.Location = new System.Drawing.Point(901, 418);
            this.btn_UpdateTabla.Name = "btn_UpdateTabla";
            this.btn_UpdateTabla.Size = new System.Drawing.Size(95, 21);
            this.btn_UpdateTabla.TabIndex = 12;
            this.btn_UpdateTabla.UseVisualStyleBackColor = true;
            this.btn_UpdateTabla.Visible = false;
            this.btn_UpdateTabla.Click += new System.EventHandler(this.btn_UpdateTabla_Click);
            // 
            // Principal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1008, 641);
            this.Controls.Add(this.btn_UpdateTabla);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.lbl_countRegistros);
            this.Controls.Add(this.BtnStart);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.PbClientes);
            this.Controls.Add(this.linkLabel1);
            this.Controls.Add(this.lblNota);
            this.Controls.Add(this.TbxMessage);
            this.Controls.Add(this.DgvClientes);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.Lb_Log);
            this.Controls.Add(this.gbLlamadasEnProgreso);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "Principal";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Principal";
            this.Load += new System.EventHandler(this.Principal_Load);
            this.Shown += new System.EventHandler(this.Principal_Shown);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DgvClientes)).EndInit();
            this.gbLlamadasEnProgreso.ResumeLayout(false);
            this.gbLlamadasEnProgreso.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ListBox Lb_Log;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lblState;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblHost;
        private System.Windows.Forms.Label LblDisplayName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridView DgvClientes;
        private System.Windows.Forms.TextBox TbxMessage;
        private System.Windows.Forms.Button BtnStart;
        private System.Windows.Forms.Label lblNota;
        private System.Windows.Forms.LinkLabel linkLabel1;
        private System.Windows.Forms.Button btnRegistrar;
        private System.Windows.Forms.ProgressBar PbClientes;
        private System.Windows.Forms.GroupBox gbLlamadasEnProgreso;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ProgressBar pbLlamadas;
        private System.Windows.Forms.Button btn_Cancel;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DateTimePicker endTime;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DateTimePicker startTime;
        private System.Windows.Forms.Button btn_configPhone;
        private System.Windows.Forms.Label lbl_countRegistros;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Timer timerSchedule;
        private System.Windows.Forms.CheckBox IsAutomatic;
        private System.Windows.Forms.LinkLabel linkLabel2;
        private System.Windows.Forms.Button btn_UpdateTabla;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DateTimePicker dtpAgenda;
    }
}
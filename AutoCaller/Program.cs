﻿using System;
using System.Windows.Forms;

namespace AutoCaller
{
    static class Program
    {
        /// <summary>
        /// Punto de entrada principal para la aplicación.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            
            Ozeki.Common.LicenseManager.Instance.SetLicense(
                "OZSDK-RAM1CALL-180517-EC2BC596",
                "TUNDOjEsTVBMOjEsRzcyOTp0cnVlLE1TTEM6MSxNRkM6MSxVUDoyMDE5LjA1LjE3LFA6MjE5OS4wMS4wMXxVbXhUN0" +
                "NUTWR2bkZtVWVKRERtVldTOWpjVUxXemFweERwcFBKZERPdkpTZG1RSmpGN05JRS85LzZZQlRxcTRIM2tIMHlRU3p5" +
                "TVFWZ0p1TjBKRi80Zz09"
            );

            Application.Run(new Principal());
        }
    }
}

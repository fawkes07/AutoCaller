﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AutoCaller.Views
{
    public partial class HelpCadenas : Form
    {
        public HelpCadenas()
        {
            InitializeComponent();
        }

        private void HelpCadenas_Load(object sender, EventArgs e)
        {
            dgvCadenas.Rows.Add("[cliente.nombre]", "Nombre del cliente");
            dgvCadenas.Rows.Add("[cliente.saldo]", "Saldo que se le esta requiriendo al cliente");
        }
    }
}

﻿using System;
using System.Windows.Forms;
using  AutoCaller.ConfigManagment;
using AutoCaller.Models;

namespace AutoCaller.Views
{
    public partial class Configuracion : Form
    {
        public Configuracion()
        {
            InitializeComponent();
        }

        private void Configuracion_Load(object sender, EventArgs e)
        {
            var response = ConfigFileManagment.GetConfigurationsByFile();

            if (!response.Success)
            {
                //Se carga configuracion por defecto

                tbDisplaName.Text = "610";
                tbUserName.Text = "610";
                tbRegisterName.Text = "610";
                tbRegisterPassword.Text = "V7fXE7Q2mCFAvyG";
                tbDomainHost.Text = "192.168.1.45";
                tbDomainPort.Text = "5060";
                cbIsRegisterRequiered.Checked = true;
            }
            else
            {
                var sipSettgins = response.RObjet as MyConfigFile ?? new MyConfigFile();

                tbDisplaName.Text = sipSettgins.DisplayName;
                tbUserName.Text = sipSettgins.UserName;
                tbRegisterName.Text = sipSettgins.RegisterName;
                tbRegisterPassword.Text = sipSettgins.RegisterPassword;
                tbDomainHost.Text = sipSettgins.DomainHost;
                tbDomainPort.Text = sipSettgins.DomainPort.ToString();
                cbIsRegisterRequiered.Checked = sipSettgins.RegistrationRequired;
            }
        }
        
        private void btnGuardar_Click(object sender, EventArgs e)
        {
            int port;
            var validate = Int32.TryParse(tbDomainPort.Text, out port);

            if (!validate)
            {
                MessageBox.Show("El puerto debe ser un valor numerico entero",
                    "Puerto incorrecto", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            var result = ConfigFileManagment.SaveConffigurationFile(new MyConfigFile
            {
                DisplayName = tbDisplaName.Text,
                UserName = tbUserName.Text,
                RegisterName = tbRegisterName.Text,
                RegisterPassword = tbRegisterPassword.Text,
                DomainHost = tbDomainHost.Text,
                DomainPort = port,
                RegistrationRequired = cbIsRegisterRequiered.Checked
            });

            if (result.Success)
            {
                MessageBox.Show(
                    "Archivo guardado correctamente",
                    "Exito",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Asterisk
                );
                Dispose();
            }
            else
                MessageBox.Show(
                    result.ErrorMessage + ": " + result?.Exception?.Message,
                    "Error",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Exclamation
                );
        }
    }
}

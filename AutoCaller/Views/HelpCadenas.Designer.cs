﻿namespace AutoCaller.Views
{
    partial class HelpCadenas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvCadenas = new System.Windows.Forms.DataGridView();
            this.Comodin = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.valor = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCadenas)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvCadenas
            // 
            this.dgvCadenas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvCadenas.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Comodin,
            this.valor});
            this.dgvCadenas.Location = new System.Drawing.Point(12, 72);
            this.dgvCadenas.Name = "dgvCadenas";
            this.dgvCadenas.Size = new System.Drawing.Size(248, 150);
            this.dgvCadenas.TabIndex = 0;
            // 
            // Comodin
            // 
            this.Comodin.HeaderText = "Comodin";
            this.Comodin.Name = "Comodin";
            // 
            // valor
            // 
            this.valor.HeaderText = "Valor de sustitucion";
            this.valor.Name = "valor";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(223, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Para su comodidad, puede usar los sigueitnes";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 35);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(175, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "comodines para crear sus mensajes";
            // 
            // HelpCadenas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(277, 233);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dgvCadenas);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "HelpCadenas";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Cadenas de sustitucion";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.HelpCadenas_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvCadenas)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvCadenas;
        private System.Windows.Forms.DataGridViewTextBoxColumn Comodin;
        private System.Windows.Forms.DataGridViewTextBoxColumn valor;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
    }
}